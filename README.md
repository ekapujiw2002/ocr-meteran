# OCR METERAN

Sistem OCR untuk membaca meteran PDAM

## Spesifikasi
1. Sistem berbasis Python3 dengan hasil executable yang *preferably stand alone*
2. Sistem membaca masukan berupa gambar meteran
3. Sistem memberikan keluaran berupa hasil pembacaan angka meteran
4. Untuk kemudahan awal, maka masukan gambar sudah di-*crop* sehingga fokus pada angka meteran saja